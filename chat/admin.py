from django.contrib import admin

from chat.models import Question, Chat 


admin.site.register(Question)
admin.site.register(Chat)
