import openai
from openai import Completion

from django.shortcuts import render
from django.views import generic
from django.conf import settings

from chat.models import Chat, Question


class HomeView(generic.View):
    template_name = 'home.html'
    queryset = Chat.objects.all()

    def get(self, request):
        return render(request, self.template_name, {})
        
    def post(self, request):
        data = request.POST
        if data.get('chat'):
            chat = self.queryset.get(id=data.get('chat'))
        else:
            chat = Chat.objects.create(question=data.get('question'))
        
        question = Question.objects.create(question=data.get('question'), chat=chat)
        
        openai.api_key = settings.OPEN_API_KEY

        answer = Completion.create(
            model='text-davinci-003',
            prompt=question.question,
            temperature=0,
            max_tokens=1000,
            top_p=1.0,
            frequency_penalty=0.0,
            presence_penalty=0.0
        )

        answer = answer['choices'][0]['text'].strip()

        question.answer = answer
        question.save()

        context = {
            'chat': chat,
            'questions': chat.question_chat.all()
        }

        return render(request, self.template_name, context)


class ChatDetailView(generic.DetailView):
    queryset = Chat.objects.all()
    template_name = 'chat_detail.html'
    
    def get(self, request, id):
        chat = self.queryset.get(id=id)

        context = {'questions': chat.question_chat.all()}

        return render(request, self.template_name, context)