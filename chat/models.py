from django.db import models
from django.utils import timezone


class Chat(models.Model):
    question = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return self.question


class Question(models.Model):
    question = models.CharField(max_length=255)
    answer = models.CharField(max_length=5000, null=True, blank=True)
    chat = models.ForeignKey(Chat, related_name='question_chat', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.question
